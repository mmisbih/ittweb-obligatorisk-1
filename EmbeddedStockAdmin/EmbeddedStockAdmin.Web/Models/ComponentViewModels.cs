﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;

namespace EmbeddedStockAdmin.Web.Models
{
    public class ComponentSummaryViewModel
    {
        public int ComponentModelId { get; set; }
        public string ComponentName { get; set; }
        public string Category { get; set; }
        public List<ComponentViewModel> Components { get; set; }
    }

    public class ComponentModelViewModel
    {
        [Display(Name = "Serial Number")]
        public string SerialNbr { get; set; }

        [Display(Name = "Name")]
        public string ComponentName { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }

        [Display(Name = "Datasheet")]
        public string Datasheet { get; set; }

        [Display(Name = "Manufacturer Link")]
        public string ManufacturerLink { get; set; }

        [Display(Name = "Comment (Admin)")]
        public string AdminComment { get; set; }

        [Display(Name = "Comment (User)")]
        public string UserComment { get; set; }

        public byte[] Image { get; set; }
    }

    public class EditComponentModelViewModel
    {
        public int EditComponentModelId { get; set; }
        [Display(Name = "Serial Number")]
        public string SerialNbr { get; set; }

        [Display(Name = "Name")]
        public string ComponentName { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }

        [Display(Name = "Datasheet")]
        public string Datasheet { get; set; }

        [Display(Name = "Manufacturer Link")]
        public string ManufacturerLink { get; set; }

        [Display(Name = "Comment (Admin)")]
        public string AdminComment { get; set; }

        [Display(Name = "Comment (User)")]
        public string UserComment { get; set; }

        public byte[] Image { get; set; }
    }

    public class AddComponentViewModel
    {
        public int ComponentModelId { get; set; }

        [Required]
        [Display(Name = "Add component number")]
        public int ComponentNbr { get; set; }
    }

    public class EditComponentViewModel
    {
        public int EditComponentId { get; set; }

        [Required]
        [Display(Name = "Edit component number")]
        public int EditComponentNbr { get; set; }
    }

    public class ComponentViewModel
    {
        public int ComponentId { get; set; }

        [Required]
        [Display(Name = "Add component number")]
        public int ComponentNbr { get; set; }
        public bool IsLoaned { get; set; }
        public string ComponentName { get; set; }
    }

    public class LoanViewModel
    {
        public string StudentNbr { get; set; }
        public string Name { get; set; }
        public string PhoneNbr { get; set; }
        public int LoanComponentId { get; set; }
        public DateTime ReturnDate{ get; set; }
    }

    public class ComponentModelDetailsViewModel
    {
        public string SerialNbr { get; set; }
        public string ComponentName { get; set; }
        public string Category { get; set; }
        public string Datasheet { get; set; }
        public string ManufacturerLink { get; set; }
        public string AdminComment { get; set; }
        public string UserComment { get; set; }

        public byte[] Image { get; set; }
    }
}