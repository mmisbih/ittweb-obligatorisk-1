﻿using System.Collections.Generic;
using EmbeddedStockAdmin.Domain.Entities;
using EmbeddedStockAdmin.Domain.Abstract;
using System.Linq;

namespace EmbeddedStockAdmin.Web.Models
{
    public static class ConvertHelpers
    {
        public static List<ComponentSummaryViewModel> FromComponentModelsToSummaryVieModels(List<ComponentModel> componentModels, IComponentRepository repo)
        {
            var viewModelComponents = new List<ComponentSummaryViewModel>();

            if (componentModels.Any())
            {
                foreach (var componentModel in componentModels)
                {
                    var components = new List<ComponentViewModel>();

                    foreach (var component in componentModel.Components)
                    {
                        var componentVM = new ComponentViewModel()
                        {
                            ComponentId = component.ComponentId,
                            ComponentNbr = component.ComponentNumber,
                            IsLoaned = repo.IsLoaned(component)
                        };

                        components.Add(componentVM);
                    }

                    var summary = new ComponentSummaryViewModel()
                    {
                        Category = componentModel.Category,
                        ComponentName = componentModel.ComponentName,
                        ComponentModelId = componentModel.ComponentModelId,
                        Components = components
                    };
                    viewModelComponents.Add(summary);
                }
                return viewModelComponents;
            }
            return null;
        }

        public static List<ComponentViewModel> SortLoanedOnly(List<ComponentSummaryViewModel> vms)
        {
            var toReturn = new List<ComponentViewModel>();
            foreach(var vm in vms)
            {
                if(vm.Components.Any(c => c.IsLoaned == true))
                {
                    foreach(var component in vm.Components)
                    {
                        if (component.IsLoaned)
                        {
                            component.ComponentName = vm.ComponentName;
                            toReturn.Add(component);
                        }
                    }
                }
            }
            return toReturn;
        }
    }
}