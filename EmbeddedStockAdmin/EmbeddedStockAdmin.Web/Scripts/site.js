﻿//Ajax calls
//Component
$('#addcomponent-form').submit(function (e) {
    e.preventDefault();
    var componentmodelId = $('#ComponentModelId').val();
    var componentnbr = $('#ComponentNbr').val();
    $.ajax({
        url: "/Component/AddComponent",
        cache: false,
        type: "post",
        datatype: "html",
        data: {
            ComponentModelId: componentmodelId,
            ComponentNbr: componentnbr
                },
        success: function (result) {
            var componentWellIdString = "#components-" + componentmodelId;
            $(componentWellIdString).append(result);
        }
    });
});

$('#deletecomponent-form').submit(function (e) {
    e.preventDefault();
    var componentId = $('#ComponentId').val();
    $.ajax({
        url: "/Component/DeleteComponent",
        cache: false,
        type: "post",
        datatype: "html",
        data: {
            componentId: componentId
        },
        success: function (result) {
            var htmlIdRemove = "#componentrow-id-" + componentId;
            $(htmlIdRemove).remove();
        }
    });
});

$('#editcomponent-form').submit(function (e) {
    e.preventDefault();
    var componentId = $('#EditComponentId').val();
    var componentNbr = $('#EditComponentNbr').val();
    $.ajax({
        url: "/Component/EditComponent",
        cache: false,
        type: "post",
        datatype: "html",
        data: {
            EditComponentId: componentId,
            EditComponentNbr: componentNbr
        },
        success: function (result) {
            //Use parsed values from server
            var newNbr = result.newnbr;
            var htmlIdChange = "#component-id-" + componentId;
            $(htmlIdChange).data('nbr', newNbr);

            //Update view
            var updateComponentId = "#component-display-" + componentId;
            $(updateComponentId).text('#' + newNbr);
        }
    });
});

$('#loancomponent-form').submit(function (e) {
    e.preventDefault();
    var loancomponentId = $('#LoanComponentId').val();
    $.ajax({
        url: "/Component/LoanComponent",
        cache: false,
        type: "post",
        datatype: "html",
        data: $(this).serialize(),
        success: function (result) {
            if (!result.loaned) alert('Error, something happened');
            //Update view
            var loanBtn = "#loan-btn-id-" + loancomponentId;
            $(loanBtn).prop("disabled", true);

            var display = "Loaned!";
            $(loanBtn).text(display);
        }
    });
});

//ComponentModel
$('#addcomponentmodel-form').submit(function (e) {
    e.preventDefault();

    $.ajax({
        url: "/Component/CreateComponentModel",
        cache: false,
        type: "post",
        datatype: "html",
        data: $(this).serialize(),
        success: function (result) {
            //Redirect here (hotfix should be changed)
            window.location.replace("/Component/Index");
        }
    });
});

$('#deletecomponentmodel-form').submit(function (e) {
    e.preventDefault();
    var componentModelId = $('#DeleteComponentModelId').val();

    $.ajax({
        url: "/Component/DeleteComponentModel",
        cache: false,
        type: "post",
        datatype: "html",
        data: {
            componentModelId: componentModelId
        },
        success: function (result) {
            if (!result.deleted) alert('Error, something happened');

            window.location.replace("/Component/Index");
        }
    });
});

$('#editcomponentmodel-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
        url: "/Component/EditComponentModel",
        cache: false,
        type: "post",
        datatype: "html",
        data: $(this).serialize(),
        success: function (result) {
            window.location.replace("/Component/Index");
        }
    });
});

function getDetails(componentmodelId) {

    $.ajax({
        url: "/Component/GetDetails",
        cache: false,
        type: "post",
        datatype: "html",
        data: { componentmodelId: componentmodelId },
        success: function (result) {
            $('#detailspartial').html(result);
        }
    });

    $('#detailsComponent').modal('show');
}

function getComponentModel(componentmodelId) {
    $("#EditComponentModelId").val(componentmodelId);

    $.ajax({
        url: "/Component/GetComponentModel",
        cache: false,
        type: "post",
        datatype: "html",
        data: { componentmodelId: componentmodelId },
        success: function (result) {
            $('#editcomponentmodel-partial').html(result);
        }
    });

    $('#editComponentModel').modal('show');
}

//Set ids on modal popups functions
function setComponentModelIdOnModal(id) {
    $("#ComponentModelId").val(id);
}

function setComponentIdOnDeleteModal(id) {
    $("#DeleteComponentModelId").val(id);
}

function setComponentIdOnModal(id) {
    $("#ComponentId").val(id);
}

function setComponentIdOnLoanModal(id) {
    $("#LoanComponentId").val(id);
}

function setComponentIdAndNbrOnModalEdit(caller) {
    var id = $(caller).data('id');
    var nbr = $(caller).data('nbr');

    $('#EditComponentNbr').val(nbr);
    $("#EditComponentId").val(id);
}

//Hide modal on form submit
$(document).on("click", ".component-submit", function () {
    $('#addComponent').modal('hide');
    $('#deleteComponent').modal('hide');
    $('#editComponent').modal('hide');
    $('#addComponentModel').modal('hide');
    $('#loanComponent').modal('hide');
});