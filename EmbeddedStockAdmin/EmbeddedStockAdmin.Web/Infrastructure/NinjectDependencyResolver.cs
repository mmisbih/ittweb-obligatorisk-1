﻿using EmbeddedStockAdmin.Domain.Abstract;
using EmbeddedStockAdmin.Domain.Concrete;
using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EmbeddedStockAdmin.Web.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            _kernel = kernelParam;
            this.AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        //Add custom bindings to IoC
        private void AddBindings()
        {
            //_kernel.Bind<IComponentRepository>().ToConstant(componentsMock.Object);
            _kernel.Bind<IComponentRepository>().To<EFComponentRepository>();
        }
    }
}