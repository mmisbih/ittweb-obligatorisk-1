﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EmbeddedStockAdmin.Web.Startup))]
namespace EmbeddedStockAdmin.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
