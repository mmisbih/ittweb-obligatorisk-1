﻿using EmbeddedStockAdmin.Domain.Abstract;
using EmbeddedStockAdmin.Web.Models;
using System.Web.Mvc;

namespace EmbeddedStockAdmin.Web.Controllers
{
    public class NavigationController : Controller
    {
        private IComponentRepository _componentRepo;

        public NavigationController(IComponentRepository componentRepo)
        {
            _componentRepo = componentRepo;
        }

        public PartialViewResult Menu()
        {
            return PartialView(_componentRepo.GetAllCategories());
        }

        [Authorize]
        [HttpPost]
        public ViewResult Search(string SearchString)
        {
            var componentmodels = _componentRepo.SearchComponentModels(SearchString);
            var summaryVM = ConvertHelpers.FromComponentModelsToSummaryVieModels(componentmodels, _componentRepo);
            return View(summaryVM);
        }
    }
}