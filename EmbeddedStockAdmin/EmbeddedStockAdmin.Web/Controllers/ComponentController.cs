﻿using System;
using System.Linq;
using System.Web.Mvc;
using EmbeddedStockAdmin.Domain.Entities;
using EmbeddedStockAdmin.Domain.Abstract;
using EmbeddedStockAdmin.Web.Models;

namespace EmbeddedStockAdmin.Web.Controllers
{
    [Authorize]
    public class ComponentController : Controller
    {
        private IComponentRepository _componentRepo;

        public ComponentController(IComponentRepository componentRepo)
        {
            _componentRepo = componentRepo;
        }

        [HttpGet]
        public ViewResult Index()
        {
            //Get components
            var componentModels = _componentRepo.ComponentModelsWithRelatedEntities;

            var summaryviewModels = ConvertHelpers.FromComponentModelsToSummaryVieModels(componentModels, _componentRepo);

            return View(summaryviewModels);
        }

        [HttpGet]
        public ViewResult CreateComponentModel()
        {
            return View(new ComponentModelViewModel());
        }

        [HttpPost]
        public ActionResult CreateComponentModel(ComponentModelViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var componentModel = new ComponentModel()
                {
                    SerialNbr = vm.SerialNbr,
                    ComponentName = vm.ComponentName,
                    Category = vm.Category,
                    Datasheet = vm.Datasheet,
                    ManufacturerLink = vm.ManufacturerLink,
                    AdminComment = vm.AdminComment,
                    UserComment = vm.UserComment
                };

                _componentRepo.AddComponentModel(componentModel);

                var image = new ComponentImage()
                {
                    Image = vm.Image
                };

                var componentsummaryVM = new ComponentSummaryViewModel()
                {
                    Category = vm.Category,
                    ComponentName = vm.ComponentName,
                };

                return Json(new { deleted = true });
            }
            return Json(new { deleted = false });
        }

        [HttpPost]
        public ActionResult DeleteComponentModel(int componentModelId)
        {
            if (ModelState.IsValid)
            {
                _componentRepo.DeleteModelAndRelatedComponents(componentModelId);
                return Json(new { deleted = true });
            }

            return Json(new { deleted = false });
        }

        //return some partial result
        [HttpPost]
        public ActionResult AddComponent(AddComponentViewModel vm)
        {
            if (ModelState.IsValid)
            {
                int componentId = _componentRepo.AddComponent(vm.ComponentNbr, vm.ComponentModelId);

                var componentVM = new ComponentViewModel()
                {
                    ComponentId = componentId,
                    ComponentNbr = vm.ComponentNbr
                };

                return PartialView("~/Views/Component/_ComponentModal.cshtml", componentVM);
            }

            return null;
        }

        [HttpPost]
        public ActionResult DeleteComponent(int componentId)
        {
            if (ModelState.IsValid)
            {
                _componentRepo.DeleteComponent(componentId);
                return Json(new { deleted = true });
            }

            return RedirectToAction("Index", "Component");
        }

        [HttpPost]
        public ActionResult EditComponent(EditComponentViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var component = _componentRepo.GetComponent(vm.EditComponentId);
                component.ComponentNumber = vm.EditComponentNbr;

                _componentRepo.EditComponent(component);
                return Json(new { newnbr = vm.EditComponentNbr });
            }

            return RedirectToAction("Index", "Component");
        }

        [HttpGet]
        public ViewResult ByCategory(string id)
        {
            //Get componentmodels by category id
            var componentmodels = _componentRepo.GetComponentModelsForCategory(id);
            var summaryViewModels = ConvertHelpers.FromComponentModelsToSummaryVieModels(componentmodels, _componentRepo);
            ViewBag.Category = id;

            return View(summaryViewModels);
        }

        [HttpPost]
        public ActionResult LoanComponent(LoanViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var student = new Student()
                {
                    Name = vm.Name,
                    PhoneNbr = vm.PhoneNbr,
                    StudentNbr = vm.StudentNbr
                };

                if (_componentRepo.StudentExists(vm.StudentNbr))
                {
                    student.StudentID = _componentRepo.UpdateStudent(student);
                }

                var loan = new Loan()
                {
                    Component = _componentRepo.GetComponent(vm.LoanComponentId),
                    IsEmailSend = false,
                    LoanDate = DateTime.Now,
                    ReturnDate = vm.ReturnDate,
                    ReservationDate = DateTime.Now,
                    Student = student
                };

                _componentRepo.LoanComponent(loan);

                return Json(new { loaned = true });

            }
            return Json(new { loaned = false });
        }

        [HttpGet]
        public ViewResult Loaned()
        {
            //Get components
            var componentModels = _componentRepo.ComponentModelsWithRelatedEntities;
            var summaryviewModels = ConvertHelpers.FromComponentModelsToSummaryVieModels(componentModels, _componentRepo);
            var loanedComponents = ConvertHelpers.SortLoanedOnly(summaryviewModels);

            return View(loanedComponents.OrderBy(x => x.ComponentName).ToList());
        }

        [HttpPost]
        public ActionResult GetDetails(int componentmodelId)
        {
            if (ModelState.IsValid)
            {
                var componentModel = _componentRepo.GetComponentModel(componentmodelId);
                //var image = _componentRepo.GetImage(componentmodelId);

                var vm = new ComponentModelDetailsViewModel()
                {
                    SerialNbr = componentModel.SerialNbr,
                    ComponentName = componentModel.ComponentName,
                    Category = componentModel.Category,
                    Datasheet = componentModel.Datasheet,
                    ManufacturerLink = componentModel.ManufacturerLink,
                    AdminComment = componentModel.AdminComment,
                    UserComment = componentModel.UserComment
                };

                //if (image != null)
                //{
                //    vm.Image = image;
                //}

                return PartialView("~/Views/Component/_ComponentModelDetails.cshtml", vm);
            }
            return Json(new { error = "An error occured" });
        }

        [HttpPost]
        public ActionResult EditComponentModel(EditComponentModelViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var componentModel = _componentRepo.GetComponentModel(vm.EditComponentModelId);
                
                componentModel.AdminComment = vm.AdminComment;
                componentModel.Category = vm.Category;
                componentModel.ComponentName = vm.ComponentName;
                componentModel.Datasheet = vm.Datasheet;
                componentModel.ManufacturerLink = vm.ManufacturerLink;
                componentModel.AdminComment = vm.AdminComment;
                componentModel.UserComment = vm.UserComment;
                componentModel.SerialNbr = vm.SerialNbr;

                //TODO: UPDATE IMAGE

                _componentRepo.EditComponentModel(componentModel);

                return Json(componentModel, JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index", "Component");
        }

        [HttpPost]
        public ActionResult GetComponentModel(int componentmodelId)
        {
            if (ModelState.IsValid)
            {
                var componentModel = _componentRepo.GetComponentModel(componentmodelId);

                var vm = new EditComponentModelViewModel()
                {
                    AdminComment = componentModel.AdminComment,
                    Category = componentModel.Category,
                    ComponentName = componentModel.ComponentName,
                    Datasheet = componentModel.Datasheet,
                    ManufacturerLink = componentModel.ManufacturerLink,
                    UserComment = componentModel.UserComment,
                    EditComponentModelId = componentModel.ComponentModelId,
                    SerialNbr = componentModel.SerialNbr
                };

                //TODO: UPDATE IMAGE

                return PartialView("~/Views/Component/_UpdateComponentModel.cshtml", vm);
            }

            return Json(new { error = true });
        }
    }
}