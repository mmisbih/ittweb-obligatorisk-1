namespace EmbeddedStockAdmin.Domain.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ComponentImages",
                c => new
                    {
                        ComponentImageId = c.Int(nullable: false, identity: true),
                        ComponentId = c.Int(nullable: false),
                        Image = c.Binary(),
                        ComponentModels_ComponentModelId = c.Int(),
                    })
                .PrimaryKey(t => t.ComponentImageId)
                .ForeignKey("dbo.ComponentModels", t => t.ComponentModels_ComponentModelId)
                .Index(t => t.ComponentModels_ComponentModelId);
            
            CreateTable(
                "dbo.ComponentModels",
                c => new
                    {
                        ComponentModelId = c.Int(nullable: false, identity: true),
                        SerialNbr = c.String(),
                        ComponentName = c.String(),
                        Category = c.String(),
                        Datasheet = c.String(),
                        ManufacturerLink = c.String(),
                        AdminComment = c.String(),
                        UserComment = c.String(),
                    })
                .PrimaryKey(t => t.ComponentModelId);
            
            CreateTable(
                "dbo.Components",
                c => new
                    {
                        ComponentId = c.Int(nullable: false, identity: true),
                        ComponentNumber = c.Int(nullable: false),
                        ComponentModelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ComponentId)
                .ForeignKey("dbo.ComponentModels", t => t.ComponentModelId, cascadeDelete: true)
                .Index(t => t.ComponentModelId);
            
            CreateTable(
                "dbo.Loans",
                c => new
                    {
                        LoanID = c.Int(nullable: false, identity: true),
                        StudentId = c.Int(nullable: false),
                        ComponentId = c.Int(nullable: false),
                        LoanDate = c.DateTime(nullable: false),
                        ReturnDate = c.DateTime(nullable: false),
                        IsEmailSend = c.Boolean(nullable: false),
                        ReservationDate = c.DateTime(nullable: false),
                        ReservationId = c.String(),
                    })
                .PrimaryKey(t => t.LoanID)
                .ForeignKey("dbo.Components", t => t.ComponentId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.StudentId)
                .Index(t => t.ComponentId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentID = c.Int(nullable: false, identity: true),
                        StudentNbr = c.String(),
                        Name = c.String(),
                        PhoneNbr = c.String(),
                    })
                .PrimaryKey(t => t.StudentID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Loans", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Loans", "ComponentId", "dbo.Components");
            DropForeignKey("dbo.ComponentImages", "ComponentModels_ComponentModelId", "dbo.ComponentModels");
            DropForeignKey("dbo.Components", "ComponentModelId", "dbo.ComponentModels");
            DropIndex("dbo.Loans", new[] { "ComponentId" });
            DropIndex("dbo.Loans", new[] { "StudentId" });
            DropIndex("dbo.Components", new[] { "ComponentModelId" });
            DropIndex("dbo.ComponentImages", new[] { "ComponentModels_ComponentModelId" });
            DropTable("dbo.Students");
            DropTable("dbo.Loans");
            DropTable("dbo.Components");
            DropTable("dbo.ComponentModels");
            DropTable("dbo.ComponentImages");
        }
    }
}
