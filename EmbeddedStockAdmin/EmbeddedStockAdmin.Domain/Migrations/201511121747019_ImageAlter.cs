namespace EmbeddedStockAdmin.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImageAlter : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ComponentImages", "ComponentModels_ComponentModelId", "dbo.ComponentModels");
            DropIndex("dbo.ComponentImages", new[] { "ComponentModels_ComponentModelId" });
            RenameColumn(table: "dbo.ComponentImages", name: "ComponentModels_ComponentModelId", newName: "ComponentModelId");
            AlterColumn("dbo.ComponentImages", "ComponentModelId", c => c.Int(nullable: false));
            CreateIndex("dbo.ComponentImages", "ComponentModelId");
            AddForeignKey("dbo.ComponentImages", "ComponentModelId", "dbo.ComponentModels", "ComponentModelId", cascadeDelete: true);
            DropColumn("dbo.ComponentImages", "ComponentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ComponentImages", "ComponentId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ComponentImages", "ComponentModelId", "dbo.ComponentModels");
            DropIndex("dbo.ComponentImages", new[] { "ComponentModelId" });
            AlterColumn("dbo.ComponentImages", "ComponentModelId", c => c.Int());
            RenameColumn(table: "dbo.ComponentImages", name: "ComponentModelId", newName: "ComponentModels_ComponentModelId");
            CreateIndex("dbo.ComponentImages", "ComponentModels_ComponentModelId");
            AddForeignKey("dbo.ComponentImages", "ComponentModels_ComponentModelId", "dbo.ComponentModels", "ComponentModelId");
        }
    }
}
