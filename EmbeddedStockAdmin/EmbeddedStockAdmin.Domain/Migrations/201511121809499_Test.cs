namespace EmbeddedStockAdmin.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Components", "ComponentModelId", "dbo.ComponentModels");
            DropForeignKey("dbo.Loans", "ComponentId", "dbo.Components");
            DropForeignKey("dbo.Loans", "StudentId", "dbo.Students");
            DropForeignKey("dbo.ComponentImages", "ComponentModelId", "dbo.ComponentModels");
            DropIndex("dbo.ComponentImages", new[] { "ComponentModelId" });
            DropIndex("dbo.Components", new[] { "ComponentModelId" });
            DropIndex("dbo.Loans", new[] { "StudentId" });
            DropIndex("dbo.Loans", new[] { "ComponentId" });
            RenameColumn(table: "dbo.Components", name: "ComponentModelId", newName: "ComponentModel_ComponentModelId");
            RenameColumn(table: "dbo.Loans", name: "ComponentId", newName: "Component_ComponentId");
            RenameColumn(table: "dbo.Loans", name: "StudentId", newName: "Student_StudentID");
            RenameColumn(table: "dbo.ComponentImages", name: "ComponentModelId", newName: "ComponentModel_ComponentModelId");
            AlterColumn("dbo.ComponentImages", "ComponentModel_ComponentModelId", c => c.Int());
            AlterColumn("dbo.Components", "ComponentModel_ComponentModelId", c => c.Int());
            AlterColumn("dbo.Loans", "Student_StudentID", c => c.Int());
            AlterColumn("dbo.Loans", "Component_ComponentId", c => c.Int());
            CreateIndex("dbo.ComponentImages", "ComponentModel_ComponentModelId");
            CreateIndex("dbo.Components", "ComponentModel_ComponentModelId");
            CreateIndex("dbo.Loans", "Component_ComponentId");
            CreateIndex("dbo.Loans", "Student_StudentID");
            AddForeignKey("dbo.Components", "ComponentModel_ComponentModelId", "dbo.ComponentModels", "ComponentModelId");
            AddForeignKey("dbo.Loans", "Component_ComponentId", "dbo.Components", "ComponentId");
            AddForeignKey("dbo.Loans", "Student_StudentID", "dbo.Students", "StudentID");
            AddForeignKey("dbo.ComponentImages", "ComponentModel_ComponentModelId", "dbo.ComponentModels", "ComponentModelId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ComponentImages", "ComponentModel_ComponentModelId", "dbo.ComponentModels");
            DropForeignKey("dbo.Loans", "Student_StudentID", "dbo.Students");
            DropForeignKey("dbo.Loans", "Component_ComponentId", "dbo.Components");
            DropForeignKey("dbo.Components", "ComponentModel_ComponentModelId", "dbo.ComponentModels");
            DropIndex("dbo.Loans", new[] { "Student_StudentID" });
            DropIndex("dbo.Loans", new[] { "Component_ComponentId" });
            DropIndex("dbo.Components", new[] { "ComponentModel_ComponentModelId" });
            DropIndex("dbo.ComponentImages", new[] { "ComponentModel_ComponentModelId" });
            AlterColumn("dbo.Loans", "Component_ComponentId", c => c.Int(nullable: false));
            AlterColumn("dbo.Loans", "Student_StudentID", c => c.Int(nullable: false));
            AlterColumn("dbo.Components", "ComponentModel_ComponentModelId", c => c.Int(nullable: false));
            AlterColumn("dbo.ComponentImages", "ComponentModel_ComponentModelId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.ComponentImages", name: "ComponentModel_ComponentModelId", newName: "ComponentModelId");
            RenameColumn(table: "dbo.Loans", name: "Student_StudentID", newName: "StudentId");
            RenameColumn(table: "dbo.Loans", name: "Component_ComponentId", newName: "ComponentId");
            RenameColumn(table: "dbo.Components", name: "ComponentModel_ComponentModelId", newName: "ComponentModelId");
            CreateIndex("dbo.Loans", "ComponentId");
            CreateIndex("dbo.Loans", "StudentId");
            CreateIndex("dbo.Components", "ComponentModelId");
            CreateIndex("dbo.ComponentImages", "ComponentModelId");
            AddForeignKey("dbo.ComponentImages", "ComponentModelId", "dbo.ComponentModels", "ComponentModelId", cascadeDelete: true);
            AddForeignKey("dbo.Loans", "StudentId", "dbo.Students", "StudentID", cascadeDelete: true);
            AddForeignKey("dbo.Loans", "ComponentId", "dbo.Components", "ComponentId", cascadeDelete: true);
            AddForeignKey("dbo.Components", "ComponentModelId", "dbo.ComponentModels", "ComponentModelId", cascadeDelete: true);
        }
    }
}
