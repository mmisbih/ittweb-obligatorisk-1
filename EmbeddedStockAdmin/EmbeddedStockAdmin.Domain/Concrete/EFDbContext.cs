﻿using EmbeddedStockAdmin.Domain.Entities;
using System.Data.Entity;

namespace EmbeddedStockAdmin.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public DbSet <Component> Components { get; set; }
        public DbSet <ComponentImage> ComponentImages { get; set; }
        public DbSet <ComponentModel> ComponentModels { get; set; }
        public DbSet <Loan> Loans { get; set; }
        public DbSet <Student> Students { get; set; }
    }
}
