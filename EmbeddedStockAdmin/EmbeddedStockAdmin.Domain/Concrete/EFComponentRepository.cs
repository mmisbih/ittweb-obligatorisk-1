﻿using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;
using EmbeddedStockAdmin.Domain.Abstract;
using EmbeddedStockAdmin.Domain.Entities;
using System;

namespace EmbeddedStockAdmin.Domain.Concrete
{
    public class EFComponentRepository : IComponentRepository
    {
        private EFDbContext _context = new EFDbContext();

        public IEnumerable<ComponentModel> ComponentModels
        {
            get { return _context.ComponentModels; }
        }

        public List<ComponentModel> ComponentModelsWithRelatedEntities
        {
            get
            {
                //Eager load components with this property
                return _context.ComponentModels
                    .Include("Components")
                    .OrderBy(x => x.ComponentName)
                    .ToList();
            }
        }

        public IEnumerable<Component> Components
        {
            get { return _context.Components; }
        }

        public IEnumerable<Loan> Loans
        {
            get { return _context.Loans; }
        }

        public IEnumerable<Student> Students
        {
            get { return _context.Students; }
        }

        public int AddComponent(int componentNbr, int componentModelId)
        {
            ComponentModel cm = _context.ComponentModels.Find(componentModelId);

            var component = new Component()
            {
                ComponentModel = cm,
                ComponentNumber = componentNbr
            };

            _context.ComponentModels
                .Include("Components")
                .Where(x => x.ComponentModelId == componentModelId)
                .FirstOrDefault()
                .Components
                .Add(component);

            _context.SaveChanges();
            _context.Database.Connection.Close();

            return component.ComponentId;
        }

        public void AddComponentModel(ComponentModel ci)
        {
            _context.ComponentModels.Add(ci);
            _context.SaveChanges();
            _context.Database.Connection.Close();
        }

        public void DeleteComponent(int id)
        {
            var component = _context.Components.Find(id);

            //Need to delete loans as well (if any attached to component)
            if(_context.Loans.Any(x => x.Component.ComponentId == id))
            {
                var loan = _context.Loans.First(x => x.Component.ComponentId == id);
                _context.Loans.Remove(loan);
            }

            _context.Components.Remove(component);
            _context.SaveChanges();
            _context.Database.Connection.Close();
        }

        public void DeleteComponent(Component c)
        {
            _context.Components.Remove(c);
            _context.SaveChanges();
            _context.Database.Connection.Close();
        }

        public void DeleteModelAndRelatedComponents(int componentModelId)
        {
            //Force read to memory because of context
            var components = _context.ComponentModels
                                        .Include("Components")
                                        .Where(x => x.ComponentModelId == componentModelId)
                                        .FirstOrDefault()
                                        .Components
                                        .ToList();

            foreach (var component in components)
            {
                this.DeleteComponent(component);
            }

            var componentModel = _context.ComponentModels.Find(componentModelId);
            _context.ComponentModels.Remove(componentModel);
            _context.SaveChanges();
            _context.Database.Connection.Close();
        }

        public void EditComponent(Component c)
        {
            _context.Components.Attach(c);
            _context.Entry(c).State = EntityState.Modified;
            _context.SaveChanges();
            _context.Database.Connection.Close();
        }

        public void EditComponentModel(ComponentModel cm)
        {
            _context.ComponentModels.Attach(cm);
            _context.Entry(cm).State = EntityState.Modified;
            _context.SaveChanges();
            _context.Database.Connection.Close();
        }

        public List<string> GetAllCategories()
        {
            List<string> categories = _context.ComponentModels
                                                .Select(x => x.Category)
                                                .Distinct()
                                                .OrderBy(x => x)
                                                .ToList();
            _context.Database.Connection.Close();
            return categories;
        }

        public Component GetComponent(int? id)
        {
            Component toReturn = _context.Components.Find(id);
            _context.Database.Connection.Close();
            return toReturn;
        }

        public ComponentModel GetComponentModel(int id)
        {
            ComponentModel toReturn = _context.ComponentModels.Find(id);
            _context.Database.Connection.Close();
            return toReturn;
        }

        public List<ComponentModel> GetComponentModelsForCategory(string category)
        {
            var componentModels = _context.ComponentModels
                                        .Include("Components")
                                        .Where(x => x.Category == category)
                                        .ToList();
            _context.Database.Connection.Close();
            return componentModels;
        }

        public byte[] GetImage(int componentModelId)
        {
            var image = _context.ComponentImages
                            .Include("ComponentModel")
                            .Where(x => x.ComponentModel.ComponentModelId == componentModelId)
                            .FirstOrDefault();

            if (image.Image != null)
            {
                return image.Image;
            }

            return null;
        }

        public List<ComponentModel> GetLoanedComponents()
        {
            var loans = _context.Loans
                            .Include("Component")
                            .ToList();

            var allComponentModels = new List<ComponentModel>();

            foreach(var loan in loans)
            {
                var componentmodels = _context.ComponentModels
                                        .Include("Components")
                                        .ToList();
                foreach(var componentModel in componentmodels)
                {
                    if (componentModel.Components.Any(x => x.ComponentId == loan.Component.ComponentId))
                    {
                        componentModel.Components.RemoveAll(x => x.ComponentId != loan.Component.ComponentId);
                        allComponentModels.Add(componentModel);
                    }   
                }
            }

            return allComponentModels.Distinct().OrderBy(x => x.ComponentName).ToList();
        }

        public bool IsLoaned(Component c)
        {
            bool isLoaned = _context.Loans
                                .Include("Component")
                                .Any(x => x.Component.ComponentId == c.ComponentId);
            return isLoaned;
        }

        public void LoanComponent(Loan l)
        {
            _context.Loans.Add(l);
            _context.Students.Add(l.Student);
            _context.SaveChanges();
            _context.Database.Connection.Close();
        }

        public List<ComponentModel> SearchComponentModels(string searchstring)
        {
            return _context.ComponentModels
                    .Include("Components")
                    .Where(x => x.Category.ToLower() == searchstring.ToLower())
                    .OrderBy(x => x.Category)
                    .ToList();
        }

        public bool StudentExists(string studentNbr)
        {
            return _context.Students.Any(x => x.StudentNbr == studentNbr);
        }

        public int UpdateStudent(Student s)
        {
            var student = _context.Students
                            .Where(x => x.StudentNbr == s.StudentNbr)
                            .FirstOrDefault();

            if(s.PhoneNbr != null) student.PhoneNbr = s.PhoneNbr;
            if(s.Name != null) student.Name = s.Name;
            if(s.StudentNbr != null) student.StudentNbr = s.StudentNbr;

            _context.Students.Attach(student);
            _context.Entry(student).State = EntityState.Modified;
            _context.SaveChanges();
            _context.Database.Connection.Close();

            return student.StudentID;
        }
    }
}
