﻿using System;

namespace EmbeddedStockAdmin.Domain.Entities
{
    public class Loan
    {
        public int LoanID { get; set; }
        
        public Student Student { get; set; }
        public Component Component { get; set; }

        public DateTime LoanDate{ get; set; }
        public DateTime ReturnDate { get; set; }
        public bool IsEmailSend { get; set; }

        public DateTime ReservationDate{ get; set; }
        public string ReservationId { get; set; }
    }
}
