﻿namespace EmbeddedStockAdmin.Domain.Entities
{
    public class ComponentImage
    {
        public int ComponentImageId { get; set; }

        public ComponentModel ComponentModel { get; set; }

        public byte[] Image{ get; set; }
    }
}
