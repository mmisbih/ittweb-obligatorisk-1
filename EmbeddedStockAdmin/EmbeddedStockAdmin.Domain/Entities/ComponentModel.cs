﻿using System.Collections.Generic;

namespace EmbeddedStockAdmin.Domain.Entities
{
    public class ComponentModel
    {
        //Primary key
        public int ComponentModelId { get; set; }

        public string SerialNbr { get; set; }
        public string ComponentName { get; set; }
        public string Category { get; set; } //Enum??
        public string Datasheet { get; set; } //Binary / pdf-format?
        public string ManufacturerLink { get; set; }
        public string AdminComment { get; set; }
        public string UserComment { get; set; }

        //Eager loading components that shares information
        public List <Component> Components { get; set; }
    }
}
