﻿namespace EmbeddedStockAdmin.Domain.Entities
{
    public class Component
    {
        public int ComponentId { get; set; }
        public int ComponentNumber { get; set; }
        
        public ComponentModel ComponentModel { get; set; }
    }
}
