﻿namespace EmbeddedStockAdmin.Domain.Entities
{
    public class Student
    {
        //Primary key
        public int StudentID { get; set; }

        public string StudentNbr { get; set; }
        public string Name { get; set; }
        public string PhoneNbr { get; set; }
    }
}
