﻿using System.Collections.Generic;
using EmbeddedStockAdmin.Domain.Entities;

namespace EmbeddedStockAdmin.Domain.Abstract
{
    public interface IComponentRepository
    {
        //All tables represented in db
        IEnumerable<Component> Components { get; }
        IEnumerable<ComponentModel> ComponentModels{ get; }
        IEnumerable<Loan> Loans { get; }
        IEnumerable<Student> Students { get; }

        //Eagerly loading
        List<ComponentModel> ComponentModelsWithRelatedEntities { get; }

        //Methods for retreiving data from db
        void AddComponentModel(ComponentModel ci);
        int AddComponent(int componentNbr, int componentModelId);
        void DeleteComponent(Component c);
        void DeleteComponent(int id);
        ComponentModel GetComponentModel(int id);
        Component GetComponent(int? id);
        void EditComponent(Component c);
        void DeleteModelAndRelatedComponents(int componentModelId);
        List<string> GetAllCategories();
        List<ComponentModel> GetComponentModelsForCategory(string category);
        bool IsLoaned(Component c);
        void LoanComponent(Loan l);
        bool StudentExists(string studentNbr);
        int UpdateStudent(Student s);
        List<ComponentModel> SearchComponentModels(string searchstring);
        List<ComponentModel> GetLoanedComponents();
        byte[] GetImage(int componentModelId);
        void EditComponentModel(ComponentModel cm);
    }
}
